package com.systex.quiz.ch4.jdbc.bean;

/**
 * 一個數據表對應一個 Java 類
 * 表中的一條記錄對應 Java 類的一個物件
 * 表中的一個欄位對應 Java 類的一個屬性
 */
public class Task {
    private int id;
    private String name;
    private String description;

    public Task() {
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String emdescriptionail) {
        this.description = description;
    }


    @Override
    public String toString() {
        return "Task{" +
                ", name='" + name + '\'' +
                ", description='" + description +
                '}';
    }
}
