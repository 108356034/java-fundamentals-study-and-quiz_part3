package com.systex.quiz.ch4.jdbc.util;

import java.lang.reflect.Field;
import java.sql.*;

public class JDBCUtils {

    public static Connection getConnection() {
        String url = "jdbc:postgresql://localhost:54320/database1";
        String user = "user1";
        String password = "00000000";

        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(url, user, password);
            if (con != null) {
                System.out.println("Opened database successfully");
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return con;
    }

    public static void closeResource(Connection connection, Statement statement) {
        try {
            connection.close();
            statement.close();
            System.out.println("Closed connection successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void closeResource(Connection connection, Statement statement, ResultSet rs) {
        try {
            connection.close();
            statement.close();
            rs.close();
            System.out.println("Closed connection successfully");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static int update(String sql, Object ...args) {
        Connection connection = null;
        PreparedStatement ps = null;
        int rowsInserted = 0;
        try {
            connection = JDBCUtils.getConnection();

            if (connection != null) {
                ps = connection.prepareStatement(sql);
            }

            if (ps != null) {
                for (int i = 0; i < args.length; i++) {
                    ps.setObject(i + 1, args[i]);
                }
                rowsInserted = ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null && ps != null) {
                JDBCUtils.closeResource(connection, ps);
            }
        }

        if (rowsInserted > 0) System.out.println("A new user was inserted successfully at line "+rowsInserted);

        return rowsInserted;
    }


    public <T> T getQuery(Class<T> clazz, String sql, Object ...args) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            // 獲取資料庫連線
            connection = JDBCUtils.getConnection();

            // 預編譯 sql 語句，獲取 PreparedStatement 例項
            if (connection != null) {
                ps = connection.prepareStatement(sql);
            }

            if (ps != null) {
                // 填充佔位符
                for (int i = 0; i < args.length; i++) {
                    ps.setObject(i + 1, args[i]);
                }

                // 執行 sql 語句，得到結果集
                rs = ps.executeQuery();

                //得到結果集的元資料
                ResultSetMetaData md = rs.getMetaData();

                // 通過結果集的元資料得到 列數 columnCount、列的別名 columnLabel
                int columnCount = md.getColumnCount();
                if (rs.next()) {    // 判斷結果集下一條是否有資料(即下一行是否有效)，如果有資料，則返回 true 並指標下移
                    T t = clazz.newInstance();
                    for (int i = 0; i < columnCount; i++) { // 遍歷每一行
                        // 獲取列名
//                        String columnLabel = md.getColumnName(i + 1);
                        // 獲取列的別名，為了解決物件屬性名和列名不一致，所以將列名的別名作為物件屬性名
                        String columnLabel = md.getColumnLabel(i + 1);
                        // 獲取列值
                        Object columnVal = rs.getObject(i + 1);

//                        System.out.println(columnLabel + " : " + columnVal);

                        // 使用反射，給物件的相應屬性賦值
                        Field field = clazz.getDeclaredField(columnLabel);
                        field.setAccessible(true);
                        field.set(t, columnVal);
                    }

                    return t;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null && ps != null && rs != null) {
                JDBCUtils.closeResource(connection, ps, rs);
            }
        }

        return null;
    }
}
