package com.systex.quiz.ch01;


import com.systex.quiz.ch01.util.Person;

import java.util.*;

public class Quiz1_4 {

    public Set<Person> getUnionFrom2Lists(List<Person> list1, List<Person> list2) {
        // Todo Case
        Set<Person> newSet = new HashSet<>(list2);
        newSet.addAll(list1);
        return newSet;
    }

    public Set<Person> getIntersectionFrom2Sets(Set<Person> set1, Set<Person> set2) {
        // Todo Case
        Set<Person> newSet = new HashSet<>();
        for (Person currentSet1 : set1) {
            for (Person currentSet2 : set2) {
                if (currentSet1.equals(currentSet2)) {
                    newSet.add(currentSet1);
                }
            }
        }
        return newSet;
    }
}