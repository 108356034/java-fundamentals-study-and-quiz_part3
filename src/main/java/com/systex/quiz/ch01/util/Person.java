package com.systex.quiz.ch01.util;


public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Todo Case
    public Integer getHashCode() {
        return this.name.hashCode();
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        return getHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Person)) {
            return false;
        } else {
            return ((Person) o).getHashCode() == this.getHashCode();
        }
    }

}