package com.systex.quiz.ch01;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Quiz1_1 {

    public String getValueByIndex(List<String> list, int index) {
        // Todo Case
        return list.get(index);
    }

    public List<String> insertValueToSpecificIndex(List<String> list, int index, String value) {
        // Todo Case
        list.add(index, value);
        return list;
    }

    public List<String> updateValueByIndex(List<String> list, int index, String value) {
        // Todo Case
        list.set(index, value);
        return list;
    }

    public List<String> removeValueByIndex(List<String> list, int index) {
        // Todo Case
        list.remove(index);
        return list;
    }

    public boolean checkValueExists(List<String> list, String value) {
        // Todo Case
        return list.contains(value);
    }

    public boolean checkListEmpty(List<String> list) {
        // Todo Case
        return list.isEmpty();
    }

    public List<String> sortList(List<String> list) {
        // Todo Case
        Collections.sort(list);
        return list;
    }

    public List<String> copyList(List<String> list) {
        // Todo Case
        return new LinkedList<>(list);
    }

    public List<String> extractList(List<String> list, int fromIndex, int toIndex) {
        // Todo Case
        return list.subList(fromIndex, toIndex);
    }

    public List<String> concatList(List<String> list1, List<String> list2) {
        // Todo Case
        List<String> newList = new LinkedList<>(list1);
        newList.addAll(list2);
        return newList;
    }

    public int maxNumber(List<Integer> list) {
        // Todo Case
        return Collections.max(list);
    }

    public int sumAllNumbers(List<Integer> list) {
        // Todo Case
        Integer tempResult = 0;
        for (Integer num : list) {
            tempResult += num;
        }
        return tempResult;
    }

    public int itemCounts(List<Integer> list) {
        // Todo Case
        return list.size();
    }

}
