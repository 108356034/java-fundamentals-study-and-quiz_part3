package com.systex.quiz.ch02;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.IntStream;

public class Quiz2_1 {

    public int maxNumber(Collection<Integer> c) {
        // Todo Case
        return c.stream()
                .mapToInt(i -> i)
                .max()
                .getAsInt();
    }

    public int sumAllNumbers(Collection<Integer> c) {
        // Todo Case
        return c.stream()
                .reduce(0, Integer::sum);
//        return c.stream().reduce(0, (i, ans) -> i + ans);
    }

    public int itemCounts(Collection<Integer> c) {
        // Todo Case
        return (int) c.stream().count();
//        return c.stream().reduce(0, (i, ans) -> i + 1);
    }

    public boolean allNumbersBiggerThan10(Collection<Integer> c) {
        // Todo Case
        return c.stream().allMatch(value -> value > 10);
    }

    public boolean anyNumberBiggerThan10(Collection<Integer> c) {
        // Todo Case
        return c.stream().anyMatch(value -> value > 10);
    }

}
