package com.systex.quiz.ch02;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Quiz2_5 {
    public Map<String, Long> mapReduceWordCount(String input) {
        // TODO
        return Arrays.stream(input.split("[\\P{L}]+"))
                .collect(Collectors.groupingBy(k -> k, () -> new HashMap<String, Long>(),
                        Collectors.counting()));

//        return Arrays.stream(input.split("[\\P{L}]+"))
//                .collect(Collectors.groupingBy(Function.identity(),
//                        Collectors.counting()));
    }
}
