package com.systex.quiz.ch02;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Quiz2_2 {

    public List<Integer> squareNumbers(Collection<Integer> c) {
        // Todo Case
        return c.stream()
                .map(integer -> integer * integer)
                .collect(Collectors.toList());
    }

    public List<String> filterLongerThan3CharsWords(Collection<String> c) {
        // Todo Case
        return c.stream()
                .filter(str -> str.length() > 3)
                .collect(Collectors.toList());

    }

    public List<String> uniqueValues(Collection<String> c) {
        // Todo Case
        return c.stream()
                .distinct()
                .collect(Collectors.toList());
//        This is less efficient since the answer is looking for an unsorted list
//        return new LinkedList<String>(c.stream()
//                                       .collect(Collectors.toSet()));
    }

    public List<String> getFirst5Words(Collection<String> c) {
        // Todo Case
        return c.stream()
                .limit(5)
                .collect(Collectors.toList());
    }

    public List<String> skipFirstWords(Collection<String> c) {
        // Todo Case
        return c.stream()
                .skip(1)
                .collect(Collectors.toList());
    }

}
