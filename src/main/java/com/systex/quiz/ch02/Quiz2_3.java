package com.systex.quiz.ch02;

import com.systex.quiz.ch02.util.Grade;
import com.systex.quiz.ch02.util.PiggyBank;

import java.util.*;
import java.util.stream.Collectors;

public class Quiz2_3 {

    public int sumOddNumbers(Collection<Integer> c) {
        return c.stream()
                .filter(x -> x % 2 != 0)
                .reduce(0, Integer::sum);
//                .reduce(0, (i, ans) -> i + ans);
    }

    public Map<String, Integer> convertListToMap(List<Grade> c) {
        // Todo Case
        return c.stream()
                .collect(Collectors.toMap(Grade::getGrade, Grade::getPoint));
    }

    public List<Integer> convertMapToList(Map<String, Integer> c) {
        // Todo Case
        return c.values()
                .stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    public Map<String, List<PiggyBank>> groupPiggyBankByColor(Collection<PiggyBank> c) {
        // Todo Case
        return new HashMap<>(c.stream()
                .collect(Collectors.groupingBy(PiggyBank::getColor)));
    }
    // into another stream
    // flatMap

}
