package com.systex.quiz.ch03;

import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

public class Quiz3_3 {
    public static void airConditioner() {
        // Todo Case
        AtomicReference<Integer> roomTemperature = new AtomicReference<>(28);
        AtomicReference<Integer> acTemperature = new AtomicReference<>(20 + (int) new Random().nextInt(10));

        Thread temperatureControl = new Thread(() -> {
            while (true) {
                acTemperature.set(20 + (int) new Random().nextInt(10));
                System.out.println("隨機設定冷氣溫度 (20~30)：" + acTemperature.get());
                System.out.println("溫度計顯示：" + roomTemperature.get());
                try {
                    Thread.sleep(5 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        temperatureControl.setPriority(Thread.MAX_PRIORITY);
        temperatureControl.start();

        while (true) {
            Integer upOrDown;
            if (acTemperature.get() == roomTemperature.get()) upOrDown = 0;
            else upOrDown = (acTemperature.get() < roomTemperature.get()) ? -1 : 1;
            roomTemperature.set(roomTemperature.get() + upOrDown);

            System.out.print(roomTemperature.get() + " -> \n");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void grabMarbles() throws InterruptedException {
        // Todo Case
        Object lock = new Object();
        AtomicReference<Integer> player101Marble = new AtomicReference<>(0);
        AtomicReference<Integer> player278Marble = new AtomicReference<>(0);
        AtomicReference<Integer> totalMarble = new AtomicReference<>(20000);

        Thread player101 = new Thread(() -> {
            while (true) {
                synchronized (lock) {
                    if (totalMarble.get() > 0) {
                        player101Marble.set(player101Marble.get() + 1);
                        totalMarble.set(totalMarble.get() - 1);
                    } else break;
                }
            }
            System.out.println("101 搶到的彈珠數量：" + player101Marble.get());
        });

        Thread player278 = new Thread(() -> {
            while (true) {
                synchronized (lock) {
                    if (totalMarble.get() > 0) {
                        player278Marble.set(player278Marble.get() + 1);
                        totalMarble.set(totalMarble.get() - 1);
                    } else break;
                }
            }
            System.out.println("278 搶到的彈珠數量：" + player278Marble.get());
        });

        player101.start();
        player278.start();
        Thread.sleep(800);
        if (totalMarble.getAndSet(player101Marble.get() + player278Marble.get()) == 0) {
            System.out.println("兩人加總的彈珠數量：" + totalMarble.get());
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // For Test Purpose
        airConditioner();
//        grabMarbles();
    }
}
