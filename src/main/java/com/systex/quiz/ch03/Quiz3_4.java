package com.systex.quiz.ch03;

import com.systex.quiz.ch03.util.HorseRacing;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import static java.lang.Thread.sleep;

public class Quiz3_4 {

    public static void horseRacing() {
        System.out.println("How many horses are racing today?");
        int horseAmount = new Scanner(System.in).nextInt();
//        create an object of the JFrame class, and
//        default setVisible(false) –“it hides the component by making it invisible “
        HorseRacing horseRacingUI = new HorseRacing(horseAmount);
//        “it reveals the component by making it visible."
        horseRacingUI.setVisible(true);


        // Todo Case
        ArrayList<Thread> horses = new ArrayList<>();

        for (int horseIndex = 1; horseIndex <= horseAmount; horseIndex++) {
            int finalHorseIndex = horseIndex;
            horses.add(new Thread(() -> {
                int horsePosition = 0;
                int horseSpeed = new Random().nextInt(21);
                int horseHiddenFeature = new Random().nextInt(15 * 1000);

                try {
                    while (horsePosition < 100) {
                        horsePosition += horseSpeed;
                        horseRacingUI.setHorsePosition(finalHorseIndex, horsePosition);
                        sleep(horseHiddenFeature);
                    }
                    horseRacingUI.setMessage("Horse " + finalHorseIndex + " win");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }));
        }

        for (Thread horse : horses) {
            horse.start();
        }

    }

    public static void main(String[] args) {
        horseRacing();
    }

}