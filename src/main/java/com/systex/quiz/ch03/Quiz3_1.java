package com.systex.quiz.ch03;

import com.systex.quiz.ch03.util.PrinterA;
import com.systex.quiz.ch03.util.PrinterB;

public class Quiz3_1 {

    public void printFromThreadByExtend(String msg) {
        // Todo Case
        PrinterA printerA = new PrinterA(msg);
        printerA.start();
    }

    public void printFromThreadByImplement(String msg) {
        // Todo Case
        Thread printerB = new Thread(new PrinterB(msg));
        printerB.start();
    }

}
