package com.systex.quiz.ch03;

public class Quiz3_2 {

    public Thread happyNewYear() {
        Thread countDown = new Thread(() -> {
            // Todo Case
            for (int currentCountDownNum = 3; currentCountDownNum > 0; currentCountDownNum--) {
                System.out.print(currentCountDownNum + "...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Happy New Year!");
        });
        countDown.start();
        return countDown;
    }

    public void eatHamburger() {
        Thread burgerQueen = new Thread(() -> {
            System.out.println("Tom's burger is done!");
        });
        burgerQueen.start();

        // Todo Case
        try {
            burgerQueen.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("*Tom is eating burger, Yum-yum...");
    }
}
