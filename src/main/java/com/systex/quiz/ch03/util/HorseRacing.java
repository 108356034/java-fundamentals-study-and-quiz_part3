package com.systex.quiz.ch03.util;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HorseRacing extends JFrame {

    private Map<String, JProgressBar> horsePositions = new HashMap<>();
    private JLabel msgLabel;

    public void setHorsePosition(int horseNumber, int position) {
        String key = "Horse " + horseNumber;
        horsePositions.get(key).setValue(position);
    }

    public void setMessage(String s) {
        msgLabel.setText(s);
    }

    public HorseRacing(int horseAmount) {
        setWindow();
        setContent(horseAmount);
    }

//    框架 JFrame
    private void setWindow() {
//        set the title of the window
        this.setTitle("Horse Racing");
//        Setting the Size and Location of The Window Simultaneously
//        unit: pixel
        this.setBounds(300, 200, 500, 300);
//        specifies close Button’s nature, and
//        is called when someone clicks on it
//        JFrame.EXIT_ON_CLOSE:
//        exits the application and
//        removes the program from memory permanently
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

//    中間容器 Container
    private void setContent(int amount) {
//        construct a container object from functions setLayout()
        Container container = setLayout(amount);
//        construct component objects from function setMessagePanel()
        JPanel msgPanel = setMessagePanel();
//        construct component objects from function setTrackPanel()
        ArrayList<JPanel> trackPanels = setTrackPanel(amount);

//        將元件加入中間容器中
        container.add(msgPanel);
        for (int i = 0; i < trackPanels.size(); i++) {
            container.add(trackPanels.get(i));
        }
    }

//        make a container object for the window
    private Container setLayout(int amount) {
        int withTitleRows = amount + 1;
        //        make a container object for the window
        Container container = getContentPane();
        container.setLayout(new GridLayout(withTitleRows, 1));
        return container;
    }

//    元件 JLabel
    private JPanel setMessagePanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel label = new JLabel("Racing...");
        msgLabel = label;
        panel.add(label);
        return panel;
    }

    //    元件 JLabel
    private ArrayList<JPanel> setTrackPanel(int n) {
        ArrayList<JPanel> panels = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
            JProgressBar progressBarAsHorsePosition = new JProgressBar();
            panel.add(progressBarAsHorsePosition);

            progressBarAsHorsePosition.setStringPainted(true);
            progressBarAsHorsePosition.setString("Horse " + i);
            horsePositions.put("Horse " + i, progressBarAsHorsePosition);

            panels.add(panel);
        }
        return panels;
    }

}