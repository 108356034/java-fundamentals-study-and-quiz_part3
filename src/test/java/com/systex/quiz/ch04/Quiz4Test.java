package com.systex.quiz.ch04;

import com.systex.quiz.ch4.Quiz4;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;


public class Quiz4Test {

    static Quiz4 quiz;

    @BeforeAll
    static void beforeAll() {
        quiz = new Quiz4();
    }

    @Test
    void testInsert() {
        int rowsInserted = quiz.testInsert("Part1", "Basic Java");
        assertEquals(1, rowsInserted);
    }



}





//    SQL
//CREATE TABLE task_table (
//task_seq int NOT NULL AUTO_INCREMENT,
//task_name varchar(255) NOT NULL,
//description varchar(255),
//PRIMARY KEY (task_seq)
//);